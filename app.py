from flask import Flask
from flask import request, jsonify

app = Flask(__name__)
app.config["DEBUG"] = True


@app.route('/')
def hello_world():
    return 'Hello World!'


result = ""


@app.route('/GET', methods=['GET'])
def get_result():
    i = request.args

    def get_theint(i):
        b = int(i.get(""))
        global result
        if b % 7 == 0 and b % 9 == 0:
            result = "CN"
        elif b % 9 == 0:
            result = "N"
        elif b % 7 == 0:
            result = "C"
        else:
            result = b

    get_theint(i)

    return jsonify({"the result is": result})


if __name__ == '__main__':
    app.run()
